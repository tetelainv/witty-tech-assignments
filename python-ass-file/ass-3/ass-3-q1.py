print()
def Check():
    score = float(input("Enter Your Score here: "))
    if score <= 1.0:
        Calculate(score)
    else:
        print("SORRY; seem your score is out of range; TRY AGAIN")
        Check()

def Calculate(score):
    if score >= 0.9 and score <= 1.0:
        print("GRADE A")
    elif score >= 0.8:
        print("GRADE B")
    elif score >= 0.7:
        print("GRADE C")
    elif score >= 0.6:
        print("GRADE D")
    else:
        print("GRADE F")

Check()